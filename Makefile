.PHONY: build build-wheel check check-license format lint pyflakes pylint test yapf

MODULES_AND_PACKAGES := hatch_build.py rubber bin

build-wheel:
	# Cleanup dist by running `rm -rf dist/`
	python3 -m build
publish-wheel:
	python3 -m twine upload dist/latex-rubber-*

check: lint test

check-license:
	./tools/check-license

format:
	yapf --in-place --recursive --parallel .

lint: pyflakes pylint yapf check-license

pyflakes:
	pyflakes $(MODULES_AND_PACKAGES)

pylint:
	pylint --errors-only $(MODULES_AND_PACKAGES)

yapf:
	yapf --diff --recursive --parallel $(MODULES_AND_PACKAGES)

test:
	(cd tests && ./run.sh *)

.PHONY: gitlab-runner
gitlab-runner:
	gitlab-runner exec docker test
	gitlab-runner exec docker lint
